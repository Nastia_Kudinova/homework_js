## 1. Як можна сторити функцію та як ми можемо її викликати?

Наприклад, нам треба показати якесь повідомлення, коли користувач входить або виходить з системи і може ще десь.

Функції — це головні “будівельні блоки” програми. Вони дозволяють робити однакові дії багато разів без повторення коду.

Щоб створити функцію нам треба її оголосити.

Спочатку ми пишемо function — це ключове слово (keyword), яке дає зрозуміти комп’ютеру, що далі буде оголошення функції.
Потім — назву функції і список її параметрів в дужках (розділені комою). Якщо параметрів немає, ми залишаємо пусті
дужки. І нарешті, код функції, який також називають тілом функції між фігурними дужками.

Нашу нову функцію можна викликати, написавши її ім’я і дужки

## 2.Що таке оператор return в JavaScript? Як його використовувати в функціях?

Оператор return в JavaScript використовується в функціях для повернення значення з функції. Коли функція викликається з
оператором return, вона повертає вказане значення, і викликуючий код може використовувати це значення.

Оператор return також може використовуватися для припинення виконання функції та повернення з неї в будь-який момент

## 3.Що таке параметри та аргументи в функіях та в яких випадках вони використовуються?

Параметри та аргументи — це терміни, які використовуються в контексті функцій в програмуванні.

Параметри:

Оголошені визначенням функції. Параметри — це змінні, які визначаються в підписі функції та призначаються значенням при
виклику функції.

Аргументи:

Передані при виклику функції. Аргументи — це значення, які фактично передаються функції при її виклику.

Важливо розрізняти ці терміни, оскільки параметри — це змінні, оголошені у визначенні функції, а аргументи — це
конкретні значення, передані функції при виклику. Функція використовує аргументи, які ви передаєте, як значення для її
параметрів, і вона працює з цими значеннями у своєму тілі.

## 4.Як передати функцію аргументом в іншу функцію?

В JavaScript, функції є об'єктами вищого порядку, що означає, що можна використовувати їх так само, як і будь-який
інший об'єкт, наприклад, передавати їх як аргументи в інші функції. Це називається передачею функцій як аргументів.

Коли передаємо функцію як аргумент, передається не результат виклику функції, а саму функцію як змінну. Це дозволяє
передавати логіку функції в інші функції та використовувати їх для реалізації різних завдань чи операцій.


Передача функцій як аргументів дозволяє створювати більш гнучкі та загальні рішення, а також робить код більш зрозумілим
та легше супроводжуваним.




