// 1 завдання
function divideNumbers(a, b) {
 if (b === 0) {
  console.log('Ділення на нуль неможливе');
  return undefined;
 } else {
  return a / b;
 }
}

let numberOne;
do {
 numberOne = parseFloat(prompt('Введіть перше число:'));
} while (isNaN(numberOne));

let numberTwo;
do {
 numberTwo = parseFloat(prompt('Введіть друге число:'));
} while (isNaN(numberTwo));

// Виклик функції та виведення результату
const result = divideNumbers(numberOne, numberTwo);

if (result !== undefined) {
 console.log(`Результат ділення: ${result}`);
}


//2 завдання
function getValidNumber(promptMessage) {
 let userInput;
 do {
  userInput = prompt(promptMessage);
 } while (isNaN(userInput));
 return parseFloat(userInput);
}


function getValidOperation() {
 let userInput;
 do {
  userInput = prompt('Введіть математичну операцію (+, -, *, /):');
 } while (!['+', '-', '*', '/'].includes(userInput));
 return userInput;
}


const number1 = getValidNumber('Введіть перше число:');
const number2 = getValidNumber('Введіть друге число:');


const operation = getValidOperation();


const calculate = (a, b, operation) => {
 switch (operation) {
  case '+':
   return a + b;
  case '-':
   return a - b;
  case '*':
   return a * b;
  case '/':
   return a / b;
 }
};

console.log(`Результат операції ${number1} ${operation} ${number2} = ${calculate(number1, number2, operation)}`);
