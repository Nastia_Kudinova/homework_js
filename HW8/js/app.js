// Створення масиву з рядків
const stringsArray = ["travel", "hello", "eat", "ski", "lift"];

// Використання функції filter для фільтрації рядків з довжиною більше 3
const filteredStrings = stringsArray.filter(str => str.length > 3);

// Виведення кількості відфільтрованих рядків в консоль
console.log(filteredStrings.length);

// Створення масиву із 4 об'єктів
const peopleArray = [
    { name: "Іван", age: 25, sex: "чоловіча" },
    { name: "Татяна", age: 15, sex: "жіноча" },
    { name: "Сергій", age: 29, sex: "чоловіча" },
    { name: "Анна", age: 10, sex: "жіноча" }
];

// Використання функції filter для фільтрації об'єктів зі sex "чоловіча"
const malePeople = peopleArray.filter(person => person.sex === "чоловіча");

// Виведення відфільтрованого масиву в консоль
console.log(malePeople);
