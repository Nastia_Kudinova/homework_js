
const root = document.documentElement;
const changeThemeButton = document.querySelector('#theme');
const isThemeActive = localStorage.getItem('isThemeActive');

if (isThemeActive === 'true') {
    changeThemeButton.classList.add('active');
    root.style.setProperty('--bg-color', '#242424');
    root.style.setProperty('--text-color', '#F6FDFF');
    root.style.setProperty('--bg-color-price-plan', '#444444');
}

changeThemeButton.addEventListener('click', (event) => {
    event.target.classList.toggle('active');

    if (event.target.classList.contains('active')) {
        root.style.setProperty('--bg-color', '#242424');
        root.style.setProperty('--text-color', '#F6FDFF');
        root.style.setProperty('--bg-color-price-plan', '#444444');
    } else {
        root.style.setProperty('--bg-color', '#fff');
        root.style.setProperty('--text-color', '#0E0F13');
        root.style.setProperty('--bg-color-price-plan', '#FDFDFD');
    }

    localStorage.setItem('isThemeActive', event.target.classList.contains('active'));
})

