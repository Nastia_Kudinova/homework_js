function isPalindrome(str) {
    const cleanStr = str.toLowerCase().replace(/[^a-z0-9]/g, ''); // видаляємо неалфавітні символи та перетворюємо у нижній регістр
    const reversedStr = cleanStr.split('').reverse().join('');
    return cleanStr === reversedStr;
}


const result1 = isPalindrome('Yawn a more Roman way');
console.log(result1);
const result2 = isPalindrome('Hello, World!');
console.log(result2);

function checkStringLength(str, maxLength) {
    return str.length <= maxLength;
}


const example1 = checkStringLength('world time', 20);
console.log(example1);

const example2 = checkStringLength('world time id start', 10);
console.log(example2);

function calculateAge() {
    const birthDate = prompt('Введіть вашу дату народження в форматі YYYY-MM-DD:');
    const currentDate = new Date();
    const userBirthDate = new Date(birthDate);

    if (isNaN(userBirthDate)) {
        return 'Неправильний формат дати.';
    }

    const ageDiff = currentDate - userBirthDate;
    const ageDate = new Date(ageDiff);

    return Math.abs(ageDate.getUTCFullYear() - 1970);
}


const userAge = calculateAge();
console.log(`Вам ${userAge} років.`);

