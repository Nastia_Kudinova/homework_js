// Завдання 1
// Створення об'єкта product
let product = {
    name: "Товар",
    price: 550,
    discount: 35,
    calculateTotalPrice: function () {

        let discountedPrice = this.price - (this.price * this.discount) / 100;
        return discountedPrice;
    }
};


console.log("Повна ціна з урахуванням знижки:", product.calculateTotalPrice());

// Завдання 2
function greetingWithAge(person) {
    return "Привіт, мені " + person.age + " років.";
}

// Запит користувача про ім'я та вік
let userName = prompt("Введіть своє ім'я:");
let userAge = prompt("Введіть свій вік:");

// Створення об'єкта з введеними даними
let user = {
    name: userName,
    age: userAge
};

// Виклик функції та виведення результату з допомогою alert
alert(greetingWithAge(user));
