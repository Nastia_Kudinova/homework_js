
function isNumber(value) {
 return !isNaN(parseFloat(value)) && isFinite(value);
}

function getUsersNumber(promptMessage) {
 let userInput;
 do {
  userInput = prompt(promptMessage);
 } while (!isNumber(userInput));
 return parseFloat(userInput);
}

const numberOne = getUsersNumber('Введіть перше число:');
const numberTwo = getUsersNumber('Введіть друге число:');

const minNumber = Math.min(numberOne, numberTwo);
const maxNumber = Math.max(numberOne, numberTwo);


alert('Мінімальне значення: ' + minNumber + '\nМаксимальне значення: ' + maxNumber);


function isEven(number) {
 return number % 2 === 0;
}
function getNumberUser() {
 let userInput;
 do {
  userInput = prompt('Введіть парне число:');
  userInput = parseInt(userInput);
 } while (!isEven(userInput));
 return userInput;
}

const evenNumber = getNumberUser();

alert('парне число: ' + evenNumber);





