/* Теоретичні питання
1. Яке призначення методу event.preventDefault() у JavaScript?
2. В чому сенс прийому делегування подій?
3. Які ви знаєте основні події документу та вікна браузера? 

Практичне завдання:

Реалізувати перемикання вкладок (таби) на чистому Javascript.

Технічні вимоги:

- У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
- Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

 Умови:
 - При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).
*/



const tabsElement = document.querySelector('.tabs');
const tabsList = document.querySelectorAll('.tabs-title');
const tabsItems = document.querySelectorAll('.tabs-content li');

tabsElement.addEventListener('click', (event) => {
    const activeElem = event.target;

    if (activeElem.classList.contains('tabs-title')) {
        const tabName = activeElem.getAttribute('data-tab');
        tabsList.forEach((elem) => {
            elem.classList.remove('active')
        });
        tabsItems.forEach((elem) => {
            elem.classList.remove('active')
        });
        activeElem.classList.add('active');
        document.querySelector(`.tabs-content li[data-tab="${tabName}"]`).classList.add('active');
    }
});

