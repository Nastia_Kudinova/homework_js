/* 
Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)
2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
4. Яка різниця між nodeList та HTMLCollection?

Практичні завдання
 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
 Використайте 2 способи для пошуку елементів. 
 Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
 
 2. Змініть текст усіх елементів h2 на "Awesome feature".

 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
 */

const elementsFeature = document.querySelectorAll('.feature');
elementsFeature.forEach(item => {
    console.log(item);
    item.style.textAlign = 'center';
})
console.log(elementsFeature);

const elementsFeature2 = document.getElementsByClassName('feature');
console.log(elementsFeature2);




const titleElements = document.querySelectorAll('h2');
titleElements.forEach(item => {
    item.innerText = 'Awesome feature';
})


const featureTitleElements = document.querySelectorAll('.feature-title');
featureTitleElements.forEach(item => {
    item.innerText += '!'
})



 